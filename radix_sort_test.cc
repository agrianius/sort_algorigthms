#include "CppUTest/TestHarness.h"
#include "CppUTest/CommandLineTestRunner.h"
#include "radix.h"
#include "radix_huldra.hh"
#include <chrono>
#include <iostream>
#include <utility>
#include <vector>

TEST_GROUP(Radix) {};


TEST(Radix, CountBuckets) {
  constexpr std::uint8_t PassCounter = 2;
  constexpr std::uint8_t BitWidth = 4;
  auto get_key_lambda = [](auto iter) -> std::uint8_t {
    return *iter;
  };
  std::uint64_t buckets[PassCounter][1U << BitWidth] = {{0}};
  std::uint8_t values[] = {1, 128, 129};
  ::sorting::impl::CountBuckets<PassCounter, BitWidth>(
      get_key_lambda, std::begin(values), std::end(values), buckets);
  CHECK_EQUAL(buckets[0][0], 1);
  CHECK_EQUAL(buckets[0][1], 2);
  CHECK_EQUAL(buckets[1][0], 1);
  CHECK_EQUAL(buckets[1][8], 2);
}


TEST(Radix, Sort) {
  std::uint16_t array[] = {4, 3, 2, 1};
  sorting::RadixStable(std::begin(array), std::end(array));
  CHECK_EQUAL(array[0], 1);
  CHECK_EQUAL(array[1], 2);
  CHECK_EQUAL(array[2], 3);
  CHECK_EQUAL(array[3], 4);
}


TEST(Radix, SortRandomUnsigned) {
  constexpr uint32_t ArrSize = 65000000;
  std::unique_ptr<std::uint32_t[]> arr(new std::uint32_t[ArrSize]);
  std::srand(42);
  uint64_t correct_sum = 0;
  for (uint32_t i = 0; i < ArrSize; ++i) {
    arr[i] = std::rand();
    correct_sum += arr[i];
  }

  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  sorting::RadixStable(&arr[0], &arr[ArrSize]);
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cerr << "Duration for Agri radix unsigned = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
            << " ms" << std::endl;

  for (uint32_t i = 0; i < ArrSize; ++i) {
    correct_sum -= arr[i];
  }
  CHECK_EQUAL(correct_sum, 0);
  for (uint32_t i = 1; i < ArrSize; ++i) {
    if (arr[i - 1] > arr[i]) {
      FAIL("non sorted");
      break;
    }
  }
}


TEST(Radix, SortRandomSigned) {
  constexpr uint32_t ArrSize = 65000000;
  std::unique_ptr<std::int32_t[]> arr(new std::int32_t[ArrSize]);
  std::srand(42);
  int64_t correct_sum = 0;
  for (uint32_t i = 0; i < ArrSize; ++i) {
    arr[i] = std::rand();
    correct_sum += arr[i];
  }

  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  sorting::RadixStable(&arr[0], &arr[ArrSize]);
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cerr << "Duration for Agri radix signed = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
            << " ms" << std::endl;

  for (uint32_t i = 0; i < ArrSize; ++i) {
    correct_sum -= arr[i];
  }
  CHECK_EQUAL(correct_sum, 0);
  for (uint32_t i = 1; i < ArrSize; ++i) {
    if (arr[i - 1] > arr[i]) {
      FAIL("non sorted");
      break;
    }
  }
}


TEST(Radix, SortHuldraRandom) {
  constexpr uint32_t ArrSize = 65000000;
  std::vector<std::uint32_t> arr;
  arr.resize(ArrSize);
  std::srand(42);
  uint64_t correct_sum = 0;
  for (uint32_t i = 0; i < ArrSize; ++i) {
    arr[i] = std::rand();
    correct_sum += arr[i];
  }

  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  sorting::huldra::radix_sort(arr);
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cerr << "Duration for Huldra radix = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
            << " ms" << std::endl;

  for (uint32_t i = 0; i < ArrSize; ++i) {
    correct_sum -= arr[i];
  }
  CHECK_EQUAL(correct_sum, 0);
  for (uint32_t i = 1; i < ArrSize; ++i) {
    if (arr[i - 1] > arr[i]) {
      FAIL("non sorted");
      break;
    }
  }
}


int main(int argc, char **argv) {
  return CommandLineTestRunner::RunAllTests(argc, argv);
}
