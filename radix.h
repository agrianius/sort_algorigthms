#pragma once
#include <cstdint>
#include <cstdlib>
#include <iterator>
#include <memory>
#include <type_traits>

namespace sorting {
namespace impl {

constexpr std::uint8_t BitsInByte = 8U;


template <std::uint8_t BitWidth, class IntType>
IntType HighBitsOfKeyToBucket(IntType key) noexcept {
  if constexpr (std::is_unsigned_v<IntType>) {
    return key;
  } else {
    return key + (1 << (BitWidth - 1));
  }
}


template <std::uint8_t PassCounter, std::uint8_t BitWidth, class KeyExtractor, class IterType>
void CountBuckets(
    KeyExtractor&& get_key,
    IterType src_begin, IterType src_end,
    std::uint64_t (&buckets)[PassCounter][1U << BitWidth])
{
  for (IterType i = src_begin; i < src_end; ++i) {
    auto key = get_key(i);
    std::uint8_t pass = 0;
    if constexpr (BitWidth < sizeof(key) * BitsInByte) {
      std::uint8_t bits_left = sizeof(key) * BitsInByte;
      while (bits_left > BitWidth) {
        constexpr decltype(key) bit_mask = (1UL << BitWidth) - 1;
        std::uint64_t bucket_num = key & bit_mask;
        ++buckets[pass][bucket_num];
        ++pass;
        key >>= BitWidth;
        bits_left -= BitWidth;
      }
    }
    std::uint64_t bucket_num = static_cast<std::uint64_t>(
        impl::HighBitsOfKeyToBucket<BitWidth>(key));
    ++buckets[pass][bucket_num];
  }
}


}  // namespace impl


template <class IterType>
void RadixStable(
    IterType begin,
    IterType end)
{
  auto get_key =
    [](IterType iter) -> typename std::iterator_traits<IterType>::value_type {
      return *iter;
    };
  using KeyExtractor = decltype(get_key);
  using value_type = typename std::iterator_traits<IterType>::value_type;
  using key_type = value_type;
  constexpr std::uint8_t BitWidth = 8;
  constexpr std::uint8_t PassCounter =
    (sizeof(key_type) * impl::BitsInByte - 1) / BitWidth + 1;
  std::uint64_t buckets[PassCounter][1UL << BitWidth] = {{0}};
  // first scan for counting of elements in each bucket
  impl::CountBuckets<PassCounter, BitWidth>(get_key, begin, end, buckets);

  // normalize values in buckets as offsets
  for (std::uint8_t pass = 0; pass < PassCounter; ++pass) {
    for (std::uint32_t i = 1; i < (1UL << BitWidth); ++i) {
      buckets[pass][i] += buckets[pass][i - 1];
    }
    for (std::uint32_t i = (1UL << BitWidth) - 1; i > 0; --i) {
      buckets[pass][i] = buckets[pass][i - 1];
    }
    buckets[pass][0] = 0;
  }

  std::unique_ptr<value_type[]> temp_array(
      new value_type[std::distance(begin, end)]);
  IterType dst_begin = &temp_array[0];
  IterType dst_end = std::next(dst_begin, std::distance(begin, end));
  constexpr key_type bit_mask = (1UL << BitWidth) - 1;
  std::uint8_t bit_shift = 0;
  std::uint8_t pass = 0;
  for (; pass != PassCounter - 1; ++pass) {
    for (IterType elem = begin; elem != end; ++elem) {
      key_type key = get_key(elem);
      std::uint64_t bucket_num = (key >> bit_shift) & bit_mask;
      auto target_pos = buckets[pass][bucket_num];
      *std::next(dst_begin, target_pos) = std::move(*elem);
      ++buckets[pass][bucket_num];
    }
    std::swap(begin, dst_begin);
    std::swap(end, dst_end);
    bit_shift += BitWidth;
  }
  for (IterType elem = begin; elem != end; ++elem) {
    key_type key = get_key(elem);
    key >>= bit_shift;
    std::uint64_t bucket_num = impl::HighBitsOfKeyToBucket<BitWidth>(key);
    auto target_pos = buckets[pass][bucket_num];
    *std::next(dst_begin, target_pos) = std::move(*elem);
    ++buckets[pass][bucket_num];
  }
  ++pass;
  if (pass & 1) {
    // in case of odd number of copies: copy to original array
    std::move(dst_begin, dst_end, begin);
  }
}


}  // namespace sorting
